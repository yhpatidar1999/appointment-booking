// Import the functions you need from the SDKs you need
const { initializeApp } = require("firebase/app");
const { getFirestore } = require("firebase/firestore");

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration

const firebaseConfig = {
  apiKey: "AIzaSyB92TkHSO7axK3AqVZayYirbl5dEGEoOmg",
  authDomain: "appointment-booking-q.firebaseapp.com",
  projectId: "appointment-booking-q",
  storageBucket: "appointment-booking-q.appspot.com",
  messagingSenderId: "434995616541",
  appId: "1:434995616541:web:01c5f1f2abe841997edff4",
};
// Initialize Firebase
const app = initializeApp(firebaseConfig);

const db = getFirestore(app);

module.exports = db;
