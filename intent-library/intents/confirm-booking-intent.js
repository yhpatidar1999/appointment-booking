const { collection, addDoc } = require("firebase/firestore");
const db = require("../../config/firebaseConf");

const addDatatoFirestore = async (car_context) => {
  let shop_number = car_context.parameters.shop_number;
  let appointment_date = car_context.parameters.appointment_date;

  console.log("car_context: ", car_context);
  console.log("shop_number: ", shop_number);
  console.log("appointment_date: ", appointment_date);

  if (shop_number === "") {
    shop_number = car_context.parameters.previous_shop_number;
  }

  if (appointment_date === "") {
    appointment_date = car_context.parameters.early_date;
  }


  const date = new Date(appointment_date).toLocaleDateString("en-us", {
    year: "numeric",
    month: "short",
    day: "numeric",
  });

  try {
    const docRef = await addDoc(collection(db, "appointments"), {
      car_make: car_context.parameters.car_make,
      car_model: car_context.parameters.car_model,
      appointment_date: date,
      shop_number: shop_number,
      phone_number: car_context.parameters.phone_number,
    });
    console.log("Document written with ID: ", docRef.id);
  } catch (e) {
    console.error("Error adding document: ", e);
  }

  return { date, shop_number };
};

const confirmBookingIntent = async (df) => {
  const car_context = df.getContext("car-context-global");
  console.log("car_conText: ", car_context);

  const car_make = car_context.parameters.car_make;
  const car_model = car_context.parameters.car_model;

  const { date, shop_number } = await addDatatoFirestore(car_context);

  console.log("appointment_date: ", date);
  console.log("shop_number: ", shop_number);

  df.setResponseText(
    `Got it. I'm booking your appointment for ${car_make} ${car_model} on ${date} at our shop ${shop_number}. Thank you for using our service.`
  );
};

module.exports = confirmBookingIntent;
