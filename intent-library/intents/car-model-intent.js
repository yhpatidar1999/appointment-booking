const db = require("../../config/firebaseConf");
const {
  collection,
  query,
  where,
  limit,
  getDocs,
} = require("firebase/firestore");

const getShopNumber = async (df) => {
  let shop_number = "";
  const car_context = df.getContext("car-context-global");
  console.log("car_conText: ", car_context);

  try {
    const car_make = car_context.parameters.car_make;
    const car_model = car_context.parameters.car_model;

    const q = query(
      collection(db, "appointments"),
      where("car_make", "==", car_make),
      where("car_model", "==", car_model),
      limit(1)
    );
    const querySnapshot = await getDocs(q);
    querySnapshot.forEach((doc) => {
      console.log(doc.id, " => ", doc.data());
      shop_number = doc.data().shop_number;
    });
  } catch (error) {
    console.log("Error getting documents: ", error);
  }

  return shop_number;
};

const carModelIntent = async (df) => {
  const previous_shop_number = await getShopNumber(df);

  df.setOutputContext("car-context-global", 10, {
    previous_shop_number,
  });

  if (previous_shop_number === "") {
    df.setResponseText(`which Shop would you like to book your car services?`);
    df.setPayload({
      richContent: [
        [
          {
            type: "chips",
            options: [
              {
                text: "44ML",
              },
              {
                text: "45MS",
              },
              {
                text: "46ML",
              },
            ],
          },
        ],
      ],
    });
  } else {
    df.setResponseText(
      `Understood. Last time you had a service in Shop Number ${previous_shop_number}. Would you prefer the same shop? `
    );
  }
};

module.exports = carModelIntent;

