const carMakeIntent = (df) => {
  const car_make = df._request.queryResult.parameters.car_make;
  console.log("car_make: ", car_make);

  df.setOutputContext("car-context-global", 10, {
    car_make,
  });

  df.setResponseText("And for which model you want to do servicing? ");
};

module.exports = carMakeIntent;
