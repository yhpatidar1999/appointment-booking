const phoneNumberIntent = (df) => {
  const car_conText = df.getContext("car-context-global");
  console.log("car_conText: ", car_conText);

  const d = new Date();
  let year = d.getFullYear();
  let month = d.getMonth();
  let day = d.getDate() + 3;

  const date = new Date(year, month, day).toLocaleDateString("en-us", {
    year: "numeric",
    month: "short",
    day: "numeric",
  });

  df.setOutputContext("car-context-global", 10, {
    early_date: date,
    appointment_date: "",
  });

  df.setResponseText(
    `Alright, The earliest time we have is on ${date}. Do you want to book an appointment on this date?`
  );
};

module.exports = phoneNumberIntent;
